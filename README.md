# Getting Started
In the project directory, you can run:
### `yarn install`  or  `npm install`

## Tasks
1. Users should be able to view the name and some basic details of the school in a table
2. The table should have pagination
3. There should be a search feature where the user can type in a school name to search for

## Run script
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## If you got error "error:0308010C:digital envelope routines::unsupported"

then run 
`export NODE_OPTIONS=--openssl-legacy-provider` => Mac OS / Window git bash

`set NODE_OPTIONS=--openssl-legacy-provider` => Windows command prompt-

`$env:NODE_OPTIONS = "--openssl-legacy-provider"` => Windows PowerShell-