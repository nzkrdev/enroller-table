import React, { useEffect, useState } from "react";
import './App.css';
import Pagination from "./components/Pagination";
import axios from "axios";

function App() {
    
  const [schools, setSchools] = useState([]);
  const [Keys, setKeys] = useState([]);
  const [Search, setSearch] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [schoolsLength] = useState(200);
  const [schoolsPerPage] = useState(20);

  function csvJSON(csvStr){
    var lines=csvStr.split("\n");
    var result = [];
    var headers=lines[0].split(",");
  
    for(var i=1;i<schoolsLength;i++){
        var obj = {};
        var currentline=lines[i].split(",");

        for(var j=0;j<headers.length;j++){
          headers[j] = headers[j] ? headers[j].replaceAll('"', '') : headers[j];
          obj[headers[j]] = currentline[j].includes('"') ? currentline[j].replaceAll('"','') : currentline[j];
        }
        result.push(obj);
  
    }
    return result;
  }

  function keyJSON(csvStr){
    var lines=csvStr.split("\n");
    var keys = [];
    var headers=lines[0].split(",");
  
    for(var j=0;j<headers.length;j++){
      headers[j] = headers[j] ? headers[j].replaceAll('"', '') : headers[j];
      keys.push(headers[j]);
    }

    return keys;
  }

  useEffect(() => {
    const fetchSchools = async () => {
      const res = await axios.get('https://catalogue.data.govt.nz/dataset/c1923d33-e781-46c9-9ea1-d9b850082be4/resource/20b7c271-fd5a-4c9e-869b-481a0e2453cd/download/schooldirectory-20-01-2023-083052.csv',{ responseType: 'blob',});
      const file = res.data;
      file.text().then((csvStr) => {

        const jsonObj = csvJSON(csvStr);
        const jsonKey = keyJSON(csvStr);

        setSchools(jsonObj);
        setKeys(jsonKey) 
      })
    }

    fetchSchools()
  },[])

  // Current page
  const indexOfLastSchool = currentPage * schoolsPerPage;
  const indexOfFirstSchool = indexOfLastSchool - schoolsPerPage;
  const currentSchools = schools.slice(indexOfFirstSchool, indexOfLastSchool);

  // Change page
  const paginate = pageNumber => {
    setCurrentPage(pageNumber);
    document.getElementById('search').value = '';
    setSearch('');
  };
// react context 
  return (
    <div className="App">
      <div className="relative overflow-x-auto">
        <h1 className="text-2xl my-10">NZ Educational Institutions</h1>
        <div className="container mx-auto my-10 px-10">

          <div className="w-60 mb-10 ml-auto text-left">
            <label className="block mb-2 text-sm font-medium text-gray-900">Search School</label>
            <input 
              id="search"
              type="text" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-2.5 py-2" 
              placeholder="Search..." 
              onChange={event => setSearch(event.target.value)} />
          </div>
          <div className="relative overflow-x-auto">
            <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="bg-gray-100 border-b">
                <tr>
                  {Keys.map((key, index) => (
                    <th 
                      className="text-sm font-medium text-gray-900 px-6 py-4 text-left" 
                      key={index}>
                        {key}
                    </th>
                  ))}            
                </tr>
              </thead>
              <tbody>
              {currentSchools.filter((school) => {
                if (Search === "") {
                  return school;
                } else if(school.Org_Name.toLowerCase().includes(Search.toLowerCase())) {
                  return school;
                }
              }).map((school, index) => (
                <tr className="border-b" key={index}>
                  {Keys.map((key, keyIndex) => (
                    <td 
                      className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap text-left" 
                      key={keyIndex}>
                      {school[key]}
                    </td>
                  ))}
                </tr>
              ))}
                
              </tbody>
            </table>
          </div>

          <Pagination
            schoolsPerPage={schoolsPerPage}
            totalSchools={schools.length}
            paginate={paginate}
            currentPage={currentPage}
            search={Search}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
