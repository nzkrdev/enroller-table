import React from 'react';

const Pagination = ({ schoolsPerPage, totalSchools, paginate, currentPage, search}) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalSchools / schoolsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav className="my-10">
      <ul className="inline-flex -space-x-px">
        {pageNumbers.map(number => (
          <li key={number}>
            <span onClick={() => { 
              paginate(number);
            }} 
               className={`px-3 py-2 cursor-pointer leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 
                          ${number === currentPage ? 'bg-gray-700 text-white' : ''}`}>
              {number}
            </span>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;